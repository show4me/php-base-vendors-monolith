FROM show4me/centos-php-base:nginx-1.15

WORKDIR /var/www/service
COPY vendor vendor
COPY composer.json composer.json
